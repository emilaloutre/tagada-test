package com.talend.test;

import com.talend.test.filter.retrocompatibility.FluentFilter;
import com.talend.test.filter.retrocompatibility.SimpleFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.talend.test.filter.PredicateFactory.wordsStartingWithLetter;
import static com.talend.test.filter.PredicateFactory.wordsWithLength;

/**
 * Bonus step : add retrocompatibility to filters.
 */
public class RetroFruitSalad {

    public static void main(String[] args) {
        List<String> fruitNames = new ArrayList<String>();
        fruitNames.add("Ananas");
        fruitNames.add("banana");
        fruitNames.add("apple");
        fruitNames.add("Pear");
        fruitNames.add("kiwi");

        List<String> favoriteFruitNames = filterWordsWith6CharactersStartingByLetterA(fruitNames);

        System.out.println(favoriteFruitNames);
    }

    public static List<String> filterWordsWith6CharactersStartingByLetterA(List<String> words) {
        SimpleFilter<String> wordsStartingByLetterA = new SimpleFilter<String>() {
            @Override
            public boolean test(String s) {
                return s.startsWith("a") || s.startsWith("A");
            }
        };

        SimpleFilter<String> wordsWith6Characters = new SimpleFilter<String>() {
            @Override
            public boolean test(String s) {
                return s.length() == 6;
            }
        };

        return FluentFilter.withList(words)
                .filter(wordsStartingByLetterA)
                .filter(wordsWith6Characters)
                .getResult();
    }

}
