package com.talend.test;

import com.talend.test.filter.Filter;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Second step : optimize code - what if one wants to add other filters ?
 */
public class FruitSalad2 {

    public static void main(String[] args) {
        List<String> fruitNames = new ArrayList<>();
        fruitNames.add("Ananas");
        fruitNames.add("banana");
        fruitNames.add("apple");
        fruitNames.add("Pear");
        fruitNames.add("kiwi");

        List<String> fruitNamesStartingByLetterA = filterWords(fruitNames, s -> s.startsWith("a") || s.startsWith("A"));

        System.out.println(fruitNamesStartingByLetterA);

        System.out.println("Example with a second filter: ");

        List<String> fruitNamesStartingByLetterAAndContainingSixLetters = filterWords(fruitNames,
                Filter.WORDS_STARTING_WITH_LETTER_A
                        .and(Filter.WORDS_WITH_SIX_LETTERS));
        System.out.println(fruitNamesStartingByLetterAAndContainingSixLetters);
    }

    public static List<String> filterWords(List<String> words, Predicate<String> condition) {
        return words.stream()
                .filter(condition)
                .collect(Collectors.toList());
    }
}
