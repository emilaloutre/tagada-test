package com.talend.test;

import java.util.ArrayList;
import java.util.List;

/**
 * Original class for this test.
 */
public class Tagada {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Ananas");
        list.add("banana");
        list.add("apple");
        list.add("Pear");
        list.add("kiwi");

        System.out.println(filter(list));
    }

    public static List<String> filter(List<String> workList) {

        for (String item : workList) {
            if (!item.startsWith("a")) {
                workList.remove(item);
            }
        }
        return workList;
    }
}
