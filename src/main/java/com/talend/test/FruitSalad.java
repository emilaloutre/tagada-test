package com.talend.test;

import java.util.ArrayList;
import java.util.List;

/**
 * First step : correct the mistake(s) so that the program works as expected.
 */
public class FruitSalad {

    public static void main(String[] args) {
        List<String> fruitNames = new ArrayList<>();
        fruitNames.add("Ananas");
        fruitNames.add("banana");
        fruitNames.add("apple");
        fruitNames.add("Pear");
        fruitNames.add("kiwi");

        System.out.println(filterWordsStartingByLetterA(fruitNames));
    }

    public static List<String> filterWordsStartingByLetterA(List<String> words) {
        List<String> filteredWords = new ArrayList<>();

        for (String item : words) {
            if (item.startsWith("a") || item.startsWith("A")) {
                filteredWords.add(item);
            }
        }
        return filteredWords;
    }
}
