package com.talend.test.filter;

import java.util.function.Predicate;

/**
 * Factory of custom predicates to be used in streams.
 */
public class PredicateFactory {

    /**
     * Returns a predicate indicating if a string has a specific length.
     * @param numberOfLetters the expected number of letters
     * @return the predicate which returns true if the string has the given length, false otherwise
     */
    public static Predicate<String> wordsWithLength(final int numberOfLetters) {
        return s -> s.length() == numberOfLetters;
    }

    /**
     * (Case-insensitive) Returns a predicate indicating if a string starts by a given letter.
     * @param letter the expected first letter (case-insensitive)
     * @return the predicate which returns true if the string starts by the given letter, false otherwise
     */
    public static Predicate<String> wordsStartingWithLetter(final String letter) {
        String lowerCaseLetter = letter.toLowerCase();
        String upperCaseLetter = letter.toUpperCase();
        return s -> s.startsWith(lowerCaseLetter) || s.startsWith(upperCaseLetter);
    }

}
