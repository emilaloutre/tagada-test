package com.talend.test.filter;

import java.util.function.Predicate;

/**
 * Filters for streamed collections.
 */
public interface Filter<T> extends Predicate<T> {

    Filter<String> WORDS_STARTING_WITH_LETTER_A = s -> s.startsWith("a") || s.startsWith("A");
    Filter<String> WORDS_WITH_SIX_LETTERS = s -> s.length() == 6;

}
