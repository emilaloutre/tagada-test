package com.talend.test.filter.retrocompatibility;

/**
 * Simple filter with only one method to implement (compatible with Java 8 and lower).
 */
public interface SimpleFilter<T> {

    boolean test(T t);
}
