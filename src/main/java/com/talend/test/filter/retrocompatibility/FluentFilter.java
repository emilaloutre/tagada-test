package com.talend.test.filter.retrocompatibility;

import java.util.ArrayList;
import java.util.List;

/**
 * Fluent filters on lists (compatible with Java 8 and lower).
 */
public class FluentFilter {

    public static <E> FilterBuilder<E> withList(List<E> list) {
        return new FilterBuilder(list);
    }

    public static class FilterBuilder<T> {
        private List<T> list;

        public FilterBuilder(List list) {
            this.list = list;
        }

        public FilterBuilder<T> filter(SimpleFilter<T> condition) {
            List<T> intermediateList = new ArrayList<T>();
            for (T item : list) {
                if (condition.test(item)) {
                    intermediateList.add(item);
                }
            }
            list = intermediateList;
            return this;
        }

        public List<T> getResult() {
            return list;
        }
    }

}
