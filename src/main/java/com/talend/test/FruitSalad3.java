package com.talend.test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.talend.test.filter.PredicateFactory.wordsStartingWithLetter;
import static com.talend.test.filter.PredicateFactory.wordsWithLength;

/**
 * Third and last step : add a new filter to get only fruit names containing 6 letters.
 */
public class FruitSalad3 {

    public static void main(String[] args) {
        List<String> fruitNames = new ArrayList<>();
        fruitNames.add("Ananas");
        fruitNames.add("banana");
        fruitNames.add("apple");
        fruitNames.add("Pear");
        fruitNames.add("kiwi");

        List<String> favoriteFruitNames = filterWordsWith6CharactersStartingByLetterA(fruitNames);

        System.out.println(favoriteFruitNames);
    }

    public static List<String> filterWordsWith6CharactersStartingByLetterA(List<String> words) {
        return words.stream()
                .filter(wordsStartingWithLetter("a")
                        .and(wordsWithLength(6)))
                .collect(Collectors.toList());
    }

}
