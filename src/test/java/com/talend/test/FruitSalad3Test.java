package com.talend.test;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class FruitSalad3Test {

    @Test
    public void testFilterFruitsNamesWithFilterFactory() {
        List<String> fruitNames = Arrays.asList("Ananas", "banana", "cherry", "apple", "starfruit");

        List<String> filteredFruitNames = FruitSalad3.filterWordsWith6CharactersStartingByLetterA(fruitNames);

        assertThat(filteredFruitNames).hasSize(1);
        assertThat(filteredFruitNames).contains("Ananas");
    }
}
