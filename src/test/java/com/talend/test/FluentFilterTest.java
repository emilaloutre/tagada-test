package com.talend.test;

import com.talend.test.filter.retrocompatibility.FluentFilter;
import com.talend.test.filter.retrocompatibility.SimpleFilter;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class FluentFilterTest {

    @Test
    public void testFilter() {
        List<String> fruitNames = Arrays.asList("Ananas", "banana", "cherry", "apple", "starfruit");
        SimpleFilter<String> filter = mock(SimpleFilter.class);
        given(filter.test("Ananas")).willReturn(true);
        given(filter.test("starfruit")).willReturn(true);

        List<String> filteredList = FluentFilter.withList(fruitNames).filter(filter).getResult();

        assertThat(filteredList).hasSize(2);
        assertThat(filteredList).contains("Ananas");
        assertThat(filteredList).contains("starfruit");
    }
}
