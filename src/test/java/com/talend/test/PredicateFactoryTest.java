package com.talend.test;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.talend.test.filter.PredicateFactory.wordsStartingWithLetter;
import static com.talend.test.filter.PredicateFactory.wordsWithLength;
import static org.assertj.core.api.Assertions.assertThat;

public class PredicateFactoryTest {

    @Test
    public void testFindWordsWithLength6() {
        List<String> fruitNames = Arrays.asList("Ananas", "banana", "cherry", "apple", "starfruit");

        List<String> filteredFruitNames = fruitNames.stream()
                .filter(wordsWithLength(6))
                .collect(Collectors.toList());

        assertThat(filteredFruitNames).hasSize(3);
        assertThat(filteredFruitNames).contains("Ananas");
        assertThat(filteredFruitNames).contains("banana");
        assertThat(filteredFruitNames).contains("cherry");
    }

    @Test
    public void testFindWordsStartingWithLetterA() {
        List<String> fruitNames = Arrays.asList("Ananas", "banana", "cherry", "apple", "starfruit");

        List<String> filteredFruitNames = fruitNames.stream()
                .filter(wordsStartingWithLetter("a"))
                .collect(Collectors.toList());

        assertThat(filteredFruitNames).hasSize(2);
        assertThat(filteredFruitNames).contains("Ananas");
        assertThat(filteredFruitNames).contains("apple");
    }

}
