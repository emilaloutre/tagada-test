package com.talend.test;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class FruitSalad2Test {

    @Test
    public void testFilterWordsWithIdentityFilter() {
        List<String> fruits = Arrays.asList("Ananas", "banana", "cherry", "apple", "starfruit");

        List<String> sameFruits = FruitSalad2.filterWords(fruits, s -> true);

        assertThat(sameFruits).isEqualTo(fruits);
    }

    @Test
    public void testFilterWords() {
        List<String> fruits = Arrays.asList("Ananas", "banana", "cherry", "apple", "starfruit");
        Predicate<String> condition = mock(Predicate.class);
        given(condition.test("Ananas")).willReturn(true);
        given(condition.test("apple")).willReturn(true);

        List<String> fruitsStartingByLetterA = FruitSalad2.filterWords(fruits, condition);

        verify(condition, times(5)).test(anyString());
        assertThat(fruitsStartingByLetterA.size()).isEqualTo(2);
        assertThat(fruitsStartingByLetterA).contains("Ananas");
        assertThat(fruitsStartingByLetterA).contains("apple");
    }

}
