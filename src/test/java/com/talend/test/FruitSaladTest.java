package com.talend.test;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class FruitSaladTest {

    @Test
    public void testFilterWordsStartingByLetterA() {
        List<String> fruitNames = Arrays.asList("Ananas", "banana", "cherry", "apple", "starfruit");

        List<String> fruitNamesStartingByLetterA = FruitSalad.filterWordsStartingByLetterA(fruitNames);

        assertThat(fruitNamesStartingByLetterA.size()).isEqualTo(2);
        assertThat(fruitNamesStartingByLetterA).contains("Ananas");
        assertThat(fruitNamesStartingByLetterA).contains("apple");
    }

}
