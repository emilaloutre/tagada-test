package com.talend.test;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class RetroFruitSaladTest {

    @Test
    public void testFluentFilterForJava7AndLower() {
        List<String> fruitNames = Arrays.asList("Ananas", "banana", "cherry", "apple", "starfruit");

        List<String> filteredFruitNames = RetroFruitSalad.filterWordsWith6CharactersStartingByLetterA(fruitNames);

        assertThat(filteredFruitNames).hasSize(1);
        assertThat(filteredFruitNames).contains("Ananas");
    }
}
